const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const User = mongoose.model('User')

exports.getById = async (id) => {
  const res = await User.findById(id)
  return res
}

exports.update = async (id, data) => {
  const user_db = await User.findById(id).select('+password')

  if (data.name) user_db.name = data.name
  if (data.email) user_db.email = data.email
  
  if (data.password) {
    if (!await bcrypt.compare(data.password, user_db.password)) {
      user_db.password = await bcrypt.hash(data.password, 10)
    }
  }

  await User.findByIdAndUpdate(id, {
    $set: {
      name: user_db.name,
      email: user_db.email,
      password: user_db.password
    }
  })
}

exports.delete = async (id) => {
  await User.findByIdAndRemove(id)
}