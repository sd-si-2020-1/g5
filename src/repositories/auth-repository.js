const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const User = mongoose.model('User')

exports.checkUser = async (email) => {
  const user = await User.findOne({email})
  if (user) {
    console.log('User => ' + user.name)
    return user
  }
}

exports.createUser = async (data) => {
  let user = new User(data)
  await user.save()
  return user.id
}

exports.authenticate = async (data) => {
  const user = await User.findOne({
    email: data.email
  }).select('+password')
  return user
}

exports.resetPassword = async (id, password) => {
  const newPassword = await bcrypt.hash(password, 10)

  await User.findByIdAndUpdate(id, {password: newPassword})
}