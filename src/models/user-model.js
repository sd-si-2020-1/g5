const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const Schema = mongoose.Schema

const UserSchema = new Schema ({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true,
    select: false
  }
})

UserSchema.pre('save', async function (next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash
  next()
})

UserSchema.methods.isValidPassword =  async function (password) {
  const isMatch = await bcrypt.compare(password, this.password)
  return isMatch ? true : false
}

module.exports = mongoose.model('User', UserSchema)