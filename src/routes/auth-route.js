const express = require('express')
const router = express.Router()

const controller = require('../controllers/auth-controller')

router.post('/signup', controller.signUp)
router.post('/signin', controller.signIn)
router.post('/logout/:id', controller.logout)
router.post('/forgot_password', controller.forgotMyPassword)
router.post('/reset_password', controller.resetMyPassword)

module.exports = router