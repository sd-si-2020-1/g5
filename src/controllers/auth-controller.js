const validationContarct = require('../validators/fluent-validators')
const repository = require('../repositories/auth-repository')
const authService = require('../services/auth-service')
const redisService = require('../services/init_redis')
const emailService = require('../services/email-service')
const crypto = require('crypto')

exports.signUp = async (req, res, next) => {
  const contract = new validationContarct()

  contract.hasMinLen(req.body.name, 3, 'O nome deve conter pelo menos 3 letras')
  contract.isEmail(req.body.email, 'E-mail inválido')
  contract.hasMinLen(req.body.password, 6, 'A senha deve conter pelo menos 6 caracteres')
  contract.isPasswordEqual(req.body.password, req.body.passwordConfirmation, 'As senhas informada não são correspondentes')

  if(!contract.isValid()) {
    res.status(400).send(contract.errors()).end()
    return
  }

  try {
    if (!(await repository.checkUser(req.body.email))) {
      const user_id = await repository.createUser({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      })

      const accessToken = await authService.generateToken({
        id: user_id,
        email: req.body.email
      })

      const refreshToken = await authService.generateRefreshToken({
        id: user_id,
        accessToken: accessToken
      })

      if (redisService.setRefreshToken(user_id, refreshToken)) {
        res.status(201).send({
          email: req.body.email,
          accessToken: accessToken,
          refreshToken: refreshToken
        })
      } else {
        res.status(404).send({
          menssage: 'Falha ao adicionar token ao redis'
        })
      }
    } else {
      res.status(400).send({
        menssage: 'Usuário já existe'
      }).end()
    }
  } catch (err) {
    res.status(500).send({
      menssage: 'Falha ao processar sua requisição'
    })
  }
}

exports.signIn = async (req, res, next) => {
  try {
    const user = await repository.authenticate({
      email: req.body.email
    })

    if (!user) {
      res.status(401).send({
        menssage: 'Usuário invalido'
      })
      return
    }

    if (!(await user.isValidPassword(req.body.password))) {
      res.status(401).send({
        menssage: "Senha invalida"
      })
      return
    }

    const accessToken = await authService.generateToken({
      id: user._id,
      email: user.email
    })

    const refreshToken = await authService.generateRefreshToken({
      id: user._id,
      accessToken: accessToken
    })

    if (redisService.setRefreshToken(user.id, refreshToken)) {
      res.status(200).send({
        email: user.email,
        accessToken: accessToken,
        refreshToken: refreshToken
      })
    } else {
      res.status(500).send({
        menssage: 'Falha ao salvar o refreshToken no redis'
      })
    }
  } catch (err) {
    res.status(500).send({
      menssage: 'Falha na autenticação'
    })
  }
}

exports.logout = async (req, res, next) => {
  try {
    const token = req.body.token || req.query.token || req.headers['x-access-token']
  
    if (!token) {
      res.status(401).send({
        menssage: 'Token não fornecido'
      })
      return
    }

    const refreshToken = await redisService.getRefreshToken(req.params.id)
    if (!refreshToken) {
      res.status(401).send({
        menssage: 'Token não encontrado'
      })
      return
    }

    const data = await authService.decodedRefreshToken(refreshToken)
    if (data.accessToken !== token) {
      res.status(401).send({
        menssage: 'Token invalido'
      })
      return
    }

    if (!(await redisService.deleteRefreshToken(req.params.id))){
      res.status(500).send({
        menssage: 'Falha ao excluir chave no redis'
      })
      return
    }

    res.status(200).send({
      menssage: 'Logout realizado com sucesso'
    })

  } catch (err) {
    res.status(500).send({
      menssage: 'Falha ao realizar logout'
    })
  }
}

exports.forgotMyPassword = async (req, res, next) => {
  try {
    const email = req.body.email
    
    if (!email) {
      res.status(400).send({
        menssage: 'Email não fornecido'
      })
      return
    }

    const user = await repository.checkUser(email)
    if (!user) {
      res.status(400).send({
        menssage: 'Usuário não encontrado'
      })
      return
    }

    const token = crypto.randomBytes(20).toString('hex')

    const now = new Date()
    now.setHours(now.getHours() + 1)

    const data = JSON.stringify({
      passwordResetToken: token,
      expiredTime: now,
      id: user._id
    })

    if (!redisService.setTokenResetPassword(email, data)) {
      res.status(400).send({
        menssage: 'Falha ao adicionar token ao redis'
      })
      return
    }

    emailService.send(email, token, user.name)
    res.status(200).send({
      menssage: 'Email enviado com sucesso'
    })

  } catch (err) {
    res.status(500).send({
      menssage: 'Falha ao enviar email para recunperar senha'
    })
  }
}

exports.resetMyPassword = async (req, res, next) => {
  const { email, token, password, passwordConfirmation } = req.body

  const contract = new validationContarct()

  contract.isEmail(email, 'E-mail invalido')
  contract.hasMinLen(password, 6, 'A senha deve conter pelo menos 6 caracteres')
  contract.isPasswordEqual(password, passwordConfirmation, 'As senhas informada não são correspondentes')

  if (!contract.isValid()) {
    res.status(400).send(contract.errors()).end()
    return
  }

  if (!token) {
    res.status(400).send({
      menssage: 'Token não fornecido'
    })
    return
  }

  try {
    const data = await redisService.getValue(email)

    if (!data) {
      res.status(400).send({
        menssage: 'Token não encontrado no redis'
      })
      return
    }

    const tokenRedis = JSON.parse(data)

    if (token != tokenRedis.passwordResetToken) {
      res.status(400).send({
        menssage: 'Token invalido'
      })
      return
    }

    if (!(await redisService.deleteRefreshToken(email))) {
      res.status(500).send({
        menssage: 'Falha ao excluir chave no redis'
      })
      return
    }

    const now = new Date()
    console.log('Now => ' + now)
    console.log('Time => ' + tokenRedis.expiredTime)

    //verificar se está comparando corretamente a hora
    if (now > tokenRedis.expiredTime) {
      res.status(400).send({
        menssage: 'Token expirado'
      })
    }

    console.log('User_id => ' + tokenRedis.id)
    //implementar uma validação de atualização
    await repository.resetPassword(tokenRedis.id, password)

    res.status(200).send({
      menssage: 'Senha atualizada com sucesso'
    })
  } catch (err) {
    res.status(500).send({
      menssage: 'Não foi possivel redefinir a senha, tente novamente'
    })
  }
}