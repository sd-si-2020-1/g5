const repository = require('../repositories/user-repository')
const ValidationContract = require('../validators/fluent-validators')

exports.getById = async (req, res, next) => {
  try {
    let data = await repository.getById(req.params.id)
    if (!data) {
      res.status(404).send({
        message: 'Usuário não encontrado'
      })
      return
    }
    res.status(200).send(data)
  }catch (e) {
    res.status(500).send({
      menssage: 'Falha ao processar sua requisição'
    })
  }
}


exports.put = async (req, res, nex) => {
  let contract = new ValidationContract()

  if (req.body.name) contract.hasMinLen(req.body.name, 3, 'O nome deve conter pelo menos 3 letras')
  if (req.body.email) contract.isEmail(req.body.email, 'E-mail inválido')
  
  if (req.body.password) {
    contract.hasMinLen(req.body.password, 6, 'A senha deve conter pelo menos 6 caracteres')
    contract.isValidPasswordConfirmation(req.body.passwordConfirmation, 'A confirmação de senha e necessaria')
    if (req.body.passwordConfirmation) {
      contract.isPasswordEqual(req.body.password, req.body.passwordConfirmation, 'As senhas informada não são correspondentes')
    }
  } 

  if(!contract.isValid()) {
    res.status(400).send(contract.errors()).end()
    return
  }

  try {
    await repository.update(req.params.id, req.body)
    
    res.status(200).send({
      menssage: 'Usuario atualizado com sucesso'
    })
  } catch (err) {
    console.log(err)
    res.status(500).send({
      menssage: "Falha ao processar sua requisição",
      error: err.codeName
    })
  }
}

exports.delete = async (req, res, next) => {
  try {
    const data = await repository.getById(req.params.id)
    if (!data) {
      res.status(404).send({
        message: 'Usuário não encontrado'
      })
      return
    }

    await repository.delete(req.params.id)
    res.status(200).send({
      menssage: 'Usuario removido com sucesso'
    })
  } catch (e) {
    res.status(500).send({
      menssage: 'Falha ao processar sua requisição',
      data: e
    })
  }
}