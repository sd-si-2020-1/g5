exports.get = (req, res, next) => {
  res.status(200).send({
    title: 'API TvTracker',
    version: '1.0.0'
  })
}