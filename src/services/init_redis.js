const redis = require('redis')

const redisClient = redis.createClient({
  port: 6379,
  host: '127.0.0.1'
})

redisClient.on('connect', () => {
  console.log('Redis client connected')
})

redisClient.on('ready', () => {
  console.log('Client connected to redis and ready to use...')
})

redisClient.on('end', () => {
  console.log('Client disconnected from redis')
})

redisClient.on('error', (err) => {
  console.log('Algo deu errado: ' + err)
})

exports.setRefreshToken = (key, value) => {
  return new Promise ((resolve, reject) => {
    redisClient.set(key, value, 'EX', 60*60, (err) => {
      if (err) {
        console.log(err)
        reject(err)
      } else {
        resolve(true)
      }
    })
  })
}

exports.getRefreshToken = (key) => {
  return new Promise((resolve, reject) => {
    redisClient.get(key, (err, value) => {
      if (err) {
        console.log(err)
        reject(err)
      } else {
        resolve(value)
      }
    })
  })
}

exports.getValue = (key) => {
  return new Promise((resolve, reject) => {
    redisClient.get(key, (err, value) => {
      if (err) {
        console.log(err)
        reject(err)
      } else {
        resolve(value)
      }
    })
  })
}

exports.deleteRefreshToken = (key) => {
  return new Promise((resolve, reject) => {
    redisClient.del(key, (err) => {
      if (err) {
        console.log(err)
        reject(err)
      } else {
        resolve(true)
      }
    })
  })
}

exports.setTokenResetPassword = (key, value) => {
  return new Promise ((resolve, reject) => {
    redisClient.set(key, value, 'EX', 60*60, (err) => {
      if (err) {
        console.log(err)
        reject(err)
      } else {
        resolve(true)
      }
    })
  })
}