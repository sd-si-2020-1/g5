const jwt = require('jsonwebtoken')

const redisService = require('./init_redis')

exports.generateToken = async (data) => {
  return jwt.sign(data, process.env.TOKEN_SECRET, {expiresIn: process.env.TOKEN_LIFE})
}

exports.generateRefreshToken = async (data) => {
  return jwt.sign(data, process.env.REFRESH_TOKEN_SECRET, {expiresIn: process.env.REFRESH_TOKEN_LIFE})
}

exports.decodedToken = async (token) => {
  let data = await jwt.verify(token, process.env.TOKEN_SECRET)
  return data
}

exports.decodedRefreshToken = async (refreshToken) => {
  let data = await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET)
  return data
}

exports.authorize = (req, res, next) => {
  let token = req.body.token || req.query.token || req.headers['x-access-token']

  if (!token) {
    res.status(401).json({
      message: 'Token não fornecido'
    })
  } else {
    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decoded) => {
      if (err) {
        if (err.message === 'jwt expired') {

          const refreshToken = await redisService.getRefreshToken(req.params.id)
          if (!refreshToken) {
            res.status(401).json({
              message: 'RefreshToken não encontrado'
            })
            return
          }

          const data = await this.decodedRefreshToken(refreshToken)

          if (data.accessToken !== token) {
            res.status(401).send({
              message: 'Token invalido'
            })
            return
          }

          if (!(await redisService.deleteRefreshToken(req.params.id))) {
            res.status(500).send({
              message: 'Falha ao excluir chave no redis'
            })
            return
          }
          console.log('Passou por aqui !!!!!!!!')

          const newToken = await this.generateToken({
            id: req.params.id,
            email: data.email
          })

          const newRefreshToken = await this.generateRefreshToken({
            id: req.params.id,
            accessToken: newToken
          })

          if (redisService.setRefreshToken(req.params.id, newRefreshToken)) {
            res.status(201).send({
              email: data.email,
              newToken: newToken,
              newRefreshToken: newRefreshToken
            })
          }
        } else {
          res.status(401).send({
            message: err.message
          })
        }
      } else {
        if (req.params.id !== decoded.id) {
          res.status(401).json({
            message: 'Token invalido'
          })
        } else {
          next()
        }
      }
    })
  }
}