const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

exports.send = async (email, token, name) => {
  await sgMail.send({
    to: email,
    from: {
      name: 'TvTracker',
      email: 'devapp221@gmail.com'
    },
    subject: 'Redefinição de senha',
    text: 'Use o seguinte token: ',
    html: `
      <h1>Olá ${name}</h1>
      <p>Use o seguite token <strong>${token}</strong> para redefinir a sua senha</p>
      <p><strong>Obs: </strong> Token valido somete por 1 hora</p>
    `
  })
}