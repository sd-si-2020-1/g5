const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const morgan = require('morgan')

const app = express()

// conecta ao banco de dados
mongoose.connect(process.env.CONNECTION_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
})

// Carrega os models
const User = require('./models/user-model')

// Carrega as rotas
const indexRoute = require('./routes/index-route')
const authRoute = require('./routes/auth-route')
const userRoute = require('./routes/user-route')

app.use(bodyParser.json({
  limit: '5mb', extended: true
}));
app.use(bodyParser.urlencoded({
  limit: '5mb', extended: true
}));
app.use(morgan('dev'))

// Define as rotas
app.use('/', indexRoute)
app.use('/auth', authRoute)
app.use('/user', userRoute)

module.exports = app