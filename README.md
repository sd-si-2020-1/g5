# 📺 TvTracker-API

#### 🚧 Em Construção ...  🚧

<!--ts-->
* [Sobre](#Sobre)
* [Funcionalidades](#Funcionalidades)
* [Demostração da Aplicação](#Demostração)
* [Pré-requisitos](#Pre-requisitos)
* [Como Executar o Projeto](Executar-projeto)
* [Tecnologias](#Tecnologias)
* [Utilitários](#Utilitários)
* [Contribuidores](#Contribuidores)
* [Autor](#Autor)
* [Licença](#Licença)
<!--te-->

## 🔎 Sobre o projeto

**TvTracker-API** e um projeto de uma api para controle e gerenciamento de **Séries**, **Filmes** e **Animes** que possibilita ao usuário criar uma lista do que pretende assistir e ir marcando os episódios que já assistiu além de poder fazer uma avaliação sobre o mesmo.

## ⚙️ Funcionalidades

- [x] SignUp
- [x] Login
- [x] Logout
- [x] Reset Password
- [ ] Definir Avatar
- [ ] Adicionar (Serie, Filme, Anime)
- [ ] Marcar Episodio como Assistido
- [ ] Avaliar Serie

## 🖥️ Demonstração da aplicação

[Assista ao vídeo de demonstração da API](https://drive.google.com/file/d/1cAhqlMdKIgwcBXWhepjdmOs0HpjIpfXX/view?usp=sharing)

## 🐣  Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua maquina as seguintes ferramentas:

* [Node js](https://nodejs.org/en/)
* [Mongo DB](https://www.mongodb.com/)
* [Docker](https://www.docker.com/)
* [Redis](https://hub.docker.com/_/redis)
* [Git](https://git-scm.com/)

Configuração do arquivo .env 

```
CONNECTION_STRING=mongodb://localhost:27017/db_tvtracker_dev

PORT=3000

TOKEN_SECRET=f5b99242-6504-4ca3-92f2-05e78e5761ef
REFRESH_TOKEN_SECRET=e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
TOKEN_LIFE=5min
REFRESH_TOKEN_LIFE=1h

SENDGRID_API_KEY=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
```

## 🚀 Como Executar o Projeto

````bash
#Inicie o MongoDB caso ele não tenha iniciado junto com o sistema
$ mongod

#Inicie o Redis
$ docker container start id_do_container
$ docker exec -it node-redis sh
$ redis-cli

#Clone este repositório
$ git Clone git@gitlab.com:sd-si-2020-1/g5.git

#Entre na pasta G5
$ cd Downloads/G5

#Instale as dependencias
$ npm install

#Execute a aplicação e modo de desenvolvimento
$ npm run dev

#O servido deve mostrar a seguinte mensagem:
API rodando na porta: 3000
Redis client connected
Client connected to redis and ready to use...
````

## 💡 Tecnologias

As seguintes tecnologias foram usadas na construção do projeto

* [Node js](https://nodejs.org/en/)
* [Express js](https://expressjs.com/pt-br/)
* [MongoDB](https://www.mongodb.com/)
* [Mongoose](https://mongoosejs.com/)
* [Json Web Token](https://jwt.io/)
* [Bcrypt](https://www.npmjs.com/package/bcrypt)
* [Dotenv](https://www.npmjs.com/package/dotenv)
* [Morgan](https://www.npmjs.com/package/morgan)
* [Sendgrid](https://www.npmjs.com/package/sendgrid)
* [Nodemon](https://www.npmjs.com/package/nodemon)
* [Docker](https://www.docker.com/)
* [Redis](https://hub.docker.com/_/redis)

## 🛠️ Utilitários

* [Insomnia](https://insomnia.rest/download/)
* [Visual Studio Code](https://code.visualstudio.com/download) 
* [Stack Edit](https://stackedit.io/app#)
* [TMDB](https://www.themoviedb.org/?language=pt-BR)
* [Abstract](https://www.abstractapi.com/user-avatar-api)

## 👨‍💻 Contribuidores

* [Marcelo Akira](https://gitlab.com/marceloakira)
* [Marcos Vinício](https://gitlab.com/MarcosGonzaga)
* [Matheus Voltolim](https://gitlab.com/mateus.voltolim)
* [Murilo Dias](https://gitlab.com/murilodias6710)

## 💪 Como contribuir para o projeto

1. Faça um **fork** do projeto
2. Crie uma nova **branch** com suas alterações : `git checkout -b my-feature`
3. Salve as alterações e crie uma mensagem de commit contando o que você vez: `git commit -m "feature: My new feature"`
4. Envie suas alterações: `git push origin my-feature`

## 🤵 Autor

* [Murilo Dias](https://gitlab.com/murilodias6710)

## 📑 Licença

Este projeto está sobre a licença [MIT]()