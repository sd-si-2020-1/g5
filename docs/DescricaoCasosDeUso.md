## Descrição dos casos de uso

Veja também: [Manual do desenvolvedor](README.md)

## Introdução

* Plantuml docs: https://plantuml.com/use-case-diagram

## Descrição UC:

```plantuml
@startuml
usecase uc1 as "UC1 - Realizar cadastro na aplicação
-- Descrição --
Como visitante ou usuário sem sregistro, gostaria de cadastrar minha conta.
-- Pré-condições --
- N/A
-- Fluxo Normal --
- visitante ou usuário sem registro, insere o primeiro nome, 
último nome, e-mail e senha, e clica no botão 'Cadastrar';
- Cadastro é realizado e o sistema mostra a mensagem de sucesso.
-- Fluxo Alternativo --
- Caso o login informado já tenha sido cadastrado:
- o sistema mostra a mensagem: 'login já cadastrado, favor utilizar um outro!'"



usecase uc2 as "UC2 - Realizar login
-- Descrição --
Como usuário registrado, gostaria de efetuar o login com a minha conta na aplicação.
-- Pré-condições --
- UC1
-- Fluxo Normal --
- Usuário com registro na aplicação, insere o e-mail e senha, e clica no botão 'Login';
- O sistema redireciona o usuário para a página principal (home).
-- Fluxo Alternativo --
- Caso o login informado esteja incorreto:
- o sistema mostra a mensagem: 'login ou senha incorretos, favor tente novamnete ou redefina sua senha!'"



usecase uc3 as "UC3 - Criar e excluir uma lista com filmes, séries e animes as uc3
-- Descrição --
Como usuário autenticado, gostaria de criar ou excluir uma lista já criada com filmes, séries, amimes na aplicação.
-- Pré-condições --
- UC2
-- Fluxo Normal --
- Usuário com registro na aplicação, insere o e-mail e senha, e clica no botão 'Login';
- O sistema redireciona o usuário para a página principal (home);
- Usuário clica em 'PROCURAR' e digita parte da descrição na barra de busca;
- O sistema filtra e mostra na tela as séries, filmes e animes correspondentes;
- Usuário clica em cima o ficheiro desejado e depois clica no botão 'Adicionar à lista'.
- o sistema adiciona o ficheiro na lista e exibe a mensagem: 'Inserido na lista com sucesso!'.
-- Fluxo Alternativo --
- N/A"



usecase uc4 as "UC4 - Realizar a marcação dos filmes, séries e animes já vistos
-- Descrição --
Como usuário autenticado, gostaria de marcar na aplicação os filmes, séries, amimes que já assisti.
-- Pré-condições --
- UC2
-- Fluxo Normal --
- Usuário com registro na aplicação, insere o e-mail e senha, e clica no botão 'Login';
- O sistema redireciona o usuário para a página principal (home);
- Usuário clica em 'FAVORITOS' e digita parte da descrição na barra de busca;
- O sistema filtra e mostra na tela as séries, filmes e animes correspondentes em sua lista;
- Usuário clica em cima da marcação do ficheiro que desejado marcar como já visto.
- o sistema realiza o preenchimento da marcaçaõ do ficheiro e o remove da lista do 
usuário e exibe a mensagem: 'Marcado como já assitido e removido da sua lista!'.
-- Fluxo Alternativo --
- Usuário clica em cima da marcação do ficheiro que desejado marcar como já visto em um ficheiro que não pertence a sua lista;
- o sistema realiza a marcação e apresenta mensagem: 'Marcado como já visto!'".



usecase uc5 as "UC5 - Adicionar e remover itens nas listas já existentes
-- Descrição --
Como usuário autenticado, gostaria de adiciona e remover filmes, séries, amimes da minha lista na aplicação.
-- Pré-condições --
- UC2
- UC3
-- Fluxo Normal --
- Usuário com registro na aplicação, insere o e-mail e senha, e clica no botão 'Login';
- O sistema redireciona o usuário para a página principal (home);
- Usuário clica em 'FAVORITOS' e digita parte da descrição na barra de busca;
- O sistema filtra e mostra na tela as séries, filmes e animes correspondentes a busca;
- Usuário clica no ficheiro que deseja adicionar a sua lista e clica no botão 'Adicionar à lista';
- o sistema adiciona o ficheiro à lista do usuário e mostra a mensagem: 'Adicionado com sucesso na sua lista!'.
-- Fluxo Alternativo --
- Usuário clica e pressiona por 5 segundos em cima do ficheiro que deseja remover da sua lista;
- o sistema realiza a remoção do ficheiro da lista do usuário;
- o sistema apresenta mensagem: 'Removido da lista com sucesso!'".



usecase uc6 as "UC6 - Realizar avaliação dos filmes, séries e animes já vistos
-- Descrição --
Como usuário autenticado, gostaria de realizar a avaliação na aplicação de filmes, séries, amimes que já assisti.
-- Pré-condições --
- UC2
-- Fluxo Normal --
- Usuário clica em 'BUSCAR' e digita parte da descrição do ficheiro na barra de busca;
- O sistema filtra e mostra na tela as séries, filmes e animes correspondentes a busca;
- Usuário clica no ficheiro que deseja realizar a avaliação e seleciona dentre uma à 5 estrelas;
- o sistema salva a avaliação do usuário e mostra a mensagem: 'Avaliação realizada com sucesso!'.
-- Fluxo Alternativo --
- N/A"
@enduml
```