# Diagrama de Casos de uso

Veja também: [Manual do desenvolvedor](README.md)

## Introdução

* Plantuml docs: https://plantuml.com/use-case-diagram

## Diagrama de caso de uso: Papéis

```plantuml
@startuml papeis
left to right direction

actor "Usuário" as usuario
note top of usuario : usuário é um visitante\n que efetuou login

rectangle App_TvTracker {
  usecase "uc1 - Realizar cadastro na aplicação" as uc1
  usecase "uc2 - Realizar login" as uc2
  usecase "uc3 - Criar e excluir uma lista com filmes, séries e animes" as uc3
  usecase "uc4 - Realizar a marcação dos filmes, séries e animes já vistos" as uc4
  usecase "uc5 - Adicionar e remover itens nas listas já existentes" as uc5
  usecase "uc6 - Realizar avaliação dos filmes, séries e animes já vistos" as uc6
}

usuario --> uc1
usuario --> uc3
uc1 <|-- uc2 : << extend >>
uc3 <|-- uc4 : << extend >>
uc3 <|-- uc5 : << extend >>
uc3 <|-- uc6 : << extend >>
@enduml

```