# Manual do desenvolvedor

## Arquitetura do backend

### Geral
```plantuml
actor usuário
node frontend <<ReactNative\nou React>> { 
}
usuário <-right-> frontend: <<celular ou web>>\ninterface
node backend <<aplicação\nNodejs>> {
}
frontend <-right-> backend: <<padrão REST>>\nHTTP
node MongoDB <<banco de dados\nbaseado em documento>> {
}
backend <-right-> MongoDB: <<MongoDB>>\nprotocolo
```

## Documentação UML
* [Diagrama de Casos de uso](CasosDeUso.md)
* [Descrição dos casos de uso](DescricaoCasosDeUso.md)
* [Diagramas de componentes](#) em breve...
* [Diagramas de sequência](#) em breve...