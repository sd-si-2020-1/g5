const http = require('http')
const app = require('../src/app')
const debug = require('debug')('tvtracker:server')

const helpers = require('./helpers')

const port = helpers.normalizePort(process.env.PORT || '3000')
app.set('port', port)

const server = http.createServer(app)
server.listen(port)
server.on('error', helpers.onError)
server.on('listening', onListening)

console.log('API rodando na porta: ' + port)

// procurar maneiras de mover para helpers.js
function onListening() {
  const addr = server.address();
    const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}